import { Md5 } from 'ts-md5/dist/md5'
import axios from 'axios'
import store from '@/store/store'

export default class Api {
  public apiUrl: string = 'https://engine.hotellook.com/api/v2/'
  public photoApiUrl: string = 'https://photo.hotellook.com/image_v2/limit/'
  public autocompleteApiUrl: string = 'https://autocomplete.travelpayouts.com/places2'
  private token: string = '95ed2753e7cbf1219ec2a20bd4f61663'
  private marker: string = '232433'

  /**
   * Good night sweet prince.
   * 
   * Promise that takes specified in `time` argument
   * @param time Time to pause in milliseconds
   */
  private sleep (time: number): Promise<any> {
    return new Promise((resolve) => setTimeout(resolve, time))
  }

  /**
   * Gets a signature for aviasales's best API ever.
   * @param query 
   */
  private getSignature (query: object): string {
    interface KeyValObject {
      val: string
      key: string
    }

    const arrayQuery: Array<KeyValObject> = []

    for (let [key, val] of Object.entries(query)) {
      arrayQuery.push({
        key: key,
        val: val
      })
    }

    const sortedArrayQuery = arrayQuery.sort((a, b) => {
      if (a.key > b.key) {
        return 1
      } else if (a.key < b.key) {
        return -1
      } else {
        return 0
      }
    })

    let queryString: string = this.token + ':' + this.marker

    sortedArrayQuery.forEach(e => {
      queryString += ':' + e.val
    })

    return Md5.hashStr(queryString) as string
  }

  /**
   * Gets a searchId for specified query.
   * 
   * Sometimes can return 403 error for no reasons. In that case it's
   * just repeats request via recursion.
   * @param query Query for searching
   */
  public async searchHotel (query: any): Promise<any> {
    let url: string = this.apiUrl + 'search/start.json?'

    query.currency = store.state.currency
    
    const signature: string = this.getSignature(query)
    
    for (let [key, val] of Object.entries(query)) {
      url += `&${key}=${val}`
    }

    url += `&marker=${this.marker}&signature=${signature}`
    
    let result: any

    await axios
      .get(url)
      .then(r => result = r)
      .catch(e => {
        if (e.message.indexOf('403') !== -1) {
          result = this.searchHotel(query)
        } else {
          throw e
        }
      })
    
    return result
  }

  /**
   * Gets hotels list for specified `searchId` and `filters`.
   * 
   * Sometimes can return 403 error for no reasons. In that case it's
   * just repeats request via recursion.
   * 
   * In case of 409 error (means search is not ready) repeats the request
   * via recursion with 2 seconds pause.
   * @param searchId SearchId returned by `searchHotel` method
   * @param filters Filters for searching (like sortBy or sortAsc)
   */
  public async searchHotelBySearchId (searchId: string, filters: object): Promise<any> {
    let url: string = this.apiUrl + 'search/getResult.json?searchId=' + searchId
    
    const signature: string = this.getSignature(Object.assign({
      searchId: searchId
    }, filters))

    for (let [key, val] of Object.entries(filters)) {
      url += `&${key}=${val}`
    }

    url += `&marker=${this.marker}&signature=${signature}`

    let result: any

    await axios
      .get(url)
      .then(r => {
        result = r
      })
      .catch(async e => {
        if (e.message.indexOf('403') !== -1) {
          result = await this.searchHotelBySearchId(searchId, filters)
        } else if (e.message.indexOf('409') !== -1) {
          await this.sleep(2 * 1000)
            .then(async _ => {
              result = await this.searchHotelBySearchId(searchId, filters)
            })
        } else {
          throw e
        }
      })

    return result
  }

  public getHotelPhotoLink (hotelId: number, photoId: number, sizeX?: number, sizeY?: number): string {
    let url: string = this.photoApiUrl + 'h' + hotelId + '_' + photoId

    if (sizeX && sizeY) {
      url += `/${sizeX}/${sizeY}.auto`
    } else if (sizeX || sizeY) {
      throw 'You must provide both X and Y sizes.'
    } else {
      url += '/200/180.auto'
    }

    return url
  }

  public async getCityAutocomplete (term: string): Promise<any> {
    let url: string = this.autocompleteApiUrl + `?term=${term}&types[]=city&locale=en`

    interface StringIndexable {
      [key: string]: string | number
    }

    const result = {
      cities: [] as Array<string>,
      cityCodes: {} as StringIndexable
    }

    await axios
      .get(url)
      .then(r => {
        const { data } = r

        data.forEach((e: any) => {
          const name = `${e.name}, ${e.country_name}`
          result.cities.push(name)
          result.cityCodes[name] = e.code as string
        })
      })
      .catch(e => {
        throw e
      })
    
    return result
  }

  public getWikipediaCityInfo (city: string): Promise<any> {
    let url: string = 'https://en.wikipedia.org/w/api.php?origin=*&action=query&prop=extracts&format=json&exintro=&titles=' + city

    return axios.get(url)
  }

  public getCityPhoto (city: string): Promise<any> {
    let url: string = `https://api.teleport.org/api/urban_areas/slug:${city.toLowerCase()}/images/`

    return axios.get(url)
  }
}
