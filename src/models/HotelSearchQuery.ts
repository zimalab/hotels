import { Dictionary } from 'vue-router/types/router'

interface IIndexable {
  [key: string]: any
}

export default class HotelSearchQuery {
  // required fields
  public iata: string = ''
  public checkIn: string = ''
  public checkOut: string = ''
  public adultsCount: number = 0

  // additional fields
  public childrenCount?: number
  public waitForResult?: number

  constructor(query: Dictionary<string | (string | null)[]>) {
    try {
      ;['checkIn', 'checkOut', 'adultsCount', 'iata'].forEach(field => {
        this.assignRequiredField(field, query[field])
      })
    } catch (e) {
      throw e
    }
  }

  public getQuery () {
    const query = {
      iata: this.iata,
      checkIn: this.checkIn,
      checkOut: this.checkOut,
      adultsCount: this.adultsCount
    }

    return query
  }

  private assignRequiredField (field: string, value: any) {
    if (value == null) {
      throw `Missed required param ${field}`
    } else {
      (this as IIndexable)[field] = value
    }
  }

  private assignNotRequiredField (field: string, value: any) {
    if (value != null) {
      (this as IIndexable)[field] = value
    }
  }
}