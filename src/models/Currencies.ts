interface currency {
  abbr: string
  name: string
  symbol: string
}

const Currencies: Array<currency> = [
  {
    abbr: 'RUB',
    name: 'RUB',
    symbol: '₽'
  },
  {
    abbr: 'USD',
    name: 'USD',
    symbol: '$'
  },
  {
    abbr: 'EUR',
    name: 'EUR',
    symbol: '€'
  }
]

export default Currencies