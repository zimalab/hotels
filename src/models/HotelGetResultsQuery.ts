interface IIndexable {
  [key: string]: any
}

/**
 * try to guess what it's used for
 */
interface HotelGetResultsQueryConstructorParams {
  searchId: string
  limit: number

  sortBy?: string
  sortAsc?: number
}

export default class HotelGetResultsQuery {
  public searchId: string = ''
  public limit: number = 10

  public sortBy?: string = 'stars'
  public sortAsc?: number = 0

  constructor(params: HotelGetResultsQueryConstructorParams) {
    try {
      this.assignRequiredField('searchId', params.searchId)
      this.assignRequiredField('limit', params.limit)

      this.assignNotRequiredField('sortBy', params.sortBy)
      this.assignNotRequiredField('sortAsc', params.sortAsc)
    } catch (e) {
      throw e
    }
  }

  private assignRequiredField (field: string, value: any) {
    if (value == null) {
      throw `Missed required param ${field}`
    } else {
      (this as IIndexable)[field] = value
    }
  }

  private assignNotRequiredField (field: string, value: any) {
    if (value != null) {
      (this as IIndexable)[field] = value
    }
  }
}