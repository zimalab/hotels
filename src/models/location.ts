export default class Location {
  public longitude: number
  public latitude: number

  constructor (lat: number, lon: number) {
    this.longitude = lon
    this.latitude = lat
  }

  toString () {
    return `${this.latitude}, ${this.longitude}`
  }

  toLatLongObject () {
    return {
      lat: this.latitude,
      lon: this.longitude
    }
  }
}