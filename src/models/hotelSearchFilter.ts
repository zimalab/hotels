export default interface SearchFilter {
  price: number
  minStars: number
  minRating: number
  additional: Array<string>
  sortBy: string
  sortAsc: number
}