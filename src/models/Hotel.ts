import Location from '@/models/location'

export default interface Hotel {
  address: string
  amenities: Array<any>
  distance: number
  fullUrl: string
  rating: number
  id: number
  location: Location
  minPrice: number
  name: string
  stars: number
  photoCount: number
  photos: Array<any>,
  photosByRoomType: Array<any>
}