function dateFormat (date: string | Date | number, shift?: number): string {
  let _date: Date

  if (typeof date === 'number') {
    _date = new Date(date)
  } else if (typeof date === 'object') {
    _date = date
  } else {
    if (date === 'today') {
      _date = new Date()
    } else if (date === 'tommorow') {
      _date = new Date(Date.now() + 1000 * 60 * 60 * 24)
    } else {
      _date = new Date(date)
    }
  }

  if (shift) {
    const oldDate = _date.getTime()
    _date = new Date(oldDate + shift * (24 * 60 * 60 * 1000))
  }

  /**
   * Adds prefix zero if digit is less then 10. For instance,
   * 9 goes to 09 while 12 will still be 12.
   * 
   * Throws an error when digit is less then 0.
   * @param digit 
   */
  const addZeroToBegin = (digit: string | number) => {
    const parsedDigit: number = parseInt(digit.toString())
    if (parsedDigit < 0) {
      throw 'Digit must be positive'
    } else if (parsedDigit < 10) {
      return `0${digit}`
    } else {
      return digit as string
    }
  }

  return `${_date.getFullYear()}-${addZeroToBegin(_date.getMonth() + 1)}-${addZeroToBegin(_date.getDate())}`
}

export default dateFormat