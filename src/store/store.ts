import Vue from 'vue'
import Vuex from 'vuex'
import Currencies from '@/models/Currencies'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    error: false as boolean,
    errorText: 'Lorem ipsum dolor sit amet',

    currency: 'USD' as string,
    currencySymbol: '$' as string
  },

  mutations: {
    setErrorMessage (state, message: string) {
      state.error = true
      state.errorText = message
    },

    /**
     * @param currency 3-digits currency abbr (e.g.: USD, EUR)
     */
    setCurrency (state, currency: string) {
      state.currency = currency
    },

    /**
     * @param currencySymbol Currency's symbol (e.g.: $)
     */
    setCurrencySymbol (state, currencySymbol: string) {
      state.currencySymbol = currencySymbol
    }
  },

  actions: {
    showError ({ commit }, message: string) {
      commit('setErrorMessage', message)
    },

    setCurrency ({ commit }, currencyAbbr: string) {
      commit('setCurrency', currencyAbbr)

      const currencySymbol = Currencies.filter(e => e.abbr === currencyAbbr)[0].symbol

      commit('setCurrencySymbol', currencySymbol)
    }
  },
})
