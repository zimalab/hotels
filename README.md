# Zimalab hotel booking

This project is based on Vue.js with TypeScript and `vue-property-decorator`

## Project setup
```
# install dependencies
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```